package ai.ecma.appchild1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppChild1Application {

    public static void main(String[] args) {
        SpringApplication.run(AppChild1Application.class, args);
    }

}
